package document;

import java.time.LocalDateTime;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;


public class CompanySeminar  extends Document {
	private String theme;
	private String speaker;
	private String company;
	
	
	public CompanySeminar(String theme, String speaker, String company, int audNumber, LocalDateTime date) {
		this.prosecutionDate = date;
		this.audNumber = audNumber;
		this.theme = theme;
		this.speaker = speaker;
		this.company=company;
	}
	
	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getSpeaker() {
		return speaker;
	}

	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	//����� ��� ��������� ���������� � ������ ���� � ����
	@Override
	public void writeToWord(String name) {
		//��������� ������������ ��������� Microsoft Ofice Word
		XWPFDocument document = new XWPFDocument();
		String theme = "������ �������� "+ this.company;
		//����� ��������� ��� ����������
		writeHeader(document, theme);
		/*
		 * ��������� ����� XWPFParagraph , �� � �������� ������.
		 * ������� ����� ��� ������
		 */
		XWPFParagraph paragraph3 = document.createParagraph();
		paragraph3.setAlignment(ParagraphAlignment.LEFT);
		paragraph3.setSpacingAfter(0);
		/*
		 * ��������� ����� XWPFRun , �� � �������� ���� ��� ������. ������� ����� ���
		 * ����
		 */
		XWPFRun run3 = paragraph3.createRun();
		run3.setFontSize(15);
		//����� ���������� ��� ������
		run3.setText("�������� : " + this.speaker);
		run3.addBreak();
		run3.setText("���� ������� : " + this.theme);
		run3.addBreak();
		//����� �������� �����������
		writeFooter(document, name);
	}

}
