package document;

import java.time.LocalDateTime;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class OpenLesson extends Document {
	private String theme;
	private String speaker;
	private String participants;
	private String lesson;
	
	public OpenLesson(String theme, String speaker, String participants, String lesson,
			int audNumber,LocalDateTime date) {
		this.prosecutionDate=date;
		this.audNumber=audNumber;
		this.theme=theme;
		this.speaker=speaker;
		this.participants=participants;
		this.lesson=lesson;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getSpeaker() {
		return speaker;
	}

	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}

	public String getParticipants() {
		return participants;
	}

	public void setParticipants(String participants) {
		this.participants = participants;
	}

	public String getLesson() {
		return lesson;
	}

	public void setLesson(String lesson) {
		this.lesson = lesson;
	}
	
	//����� ��� ��������� ���������� � ������ ���� � ����
	@Override
	public void writeToWord(String name) {
		//��������� ������������ ��������� Microsoft Ofice Word
		XWPFDocument document = new XWPFDocument();
		String theme ="������� ������ � �����" + this.lesson;
		//����� ��������� ��� ����������
		writeHeader(document,theme);
		/*
		 * ��������� ����� XWPFParagraph , �� � �������� ������.
		 * ������� ����� ��� ������
		 */
		XWPFParagraph paragraph1 = document.createParagraph();
		paragraph1.setAlignment(ParagraphAlignment.CENTER);
		/*
		 * ��������� ����� XWPFRun , �� � �������� ���� ��� ������. ������� ����� ���
		 * ����
		 */
		XWPFRun run2 = paragraph1.createRun();
		run2.setFontSize(15);
		//�����  ���������� ��� ������
		run2.setText(" �� ���� : " + this.lesson );
		run2.addBreak();
		run2.setText("��� �������� " + this.participants+ " ����(�)");
		run2.addBreak();
		run2.setText("�������� :" +this.speaker );
		//����� �������� �����������
		writeFooter(document,name);
	}
	
}
