/**
 * 
 */
package document;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.TreeSet;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 * @author Mike
 *
 */
public class DepartmentSession extends Document {

	private String title;
	private Set<String> actions = new TreeSet<String>();
	private int protocolNumber;
	private LocalDate printDocumentDate;

	public DepartmentSession(String actions, int protocolNumber, LocalDate printDocumentDate, int aud,
			LocalDateTime prosecutionDate) {
		String[] s = actions.split("\n");
		for (String s1 : s)
			if (!s1.isEmpty())
				this.actions.add(s1);
		this.protocolNumber = protocolNumber;
		this.printDocumentDate = printDocumentDate;
		this.prosecutionDate = prosecutionDate;
		this.audNumber = aud;
	}

	public DepartmentSession() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void addAction(String action) {
		this.actions.add(action);
	}

	public Set<String> getActions() {
		return actions;
	}

	public void setActions(Set<String> actions) {
		this.actions = actions;
	}

	public void addActions(Set<String> actions) {
		this.actions.addAll(actions);
	}

	public int getProtocolNumber() {
		return protocolNumber;
	}

	public void setProtocolNumber(int protocolNumber) {
		this.protocolNumber = protocolNumber;
	}

	public LocalDate getPrintDocumentDate() {
		return printDocumentDate;
	}

	public void setPrintDocumentDate(LocalDate printDocumentDate) {
		this.printDocumentDate = printDocumentDate;
	}

	// ����� ��� ��������� ���������� � ������ ���� � ����
	@Override
	public void writeToWord(String name) {
		int count = 1;
		// ��������� ������������ ��������� Microsoft Ofice Word
		XWPFDocument document = new XWPFDocument();
		// ����� ��������� ��� ����������
		writeHeader(document, "�������� ������� ��������� ���������� �� ������������� ���������");
		/*
		 * ��������� ����� XWPFParagraph , �� � �������� ������. ������� ����� ���
		 * ������
		 */
		XWPFParagraph paragraph1 = document.createParagraph();
		paragraph1.setAlignment(ParagraphAlignment.CENTER);
		/*
		 * ��������� ����� XWPFRun , �� � �������� ���� ��� ������. ������� ����� ���
		 * ����
		 */
		XWPFRun run1 = paragraph1.createRun();
		run1.setFontSize(22);
		run1.setText("������� ������");
		run1.addBreak();
		// ����� � ���� ������ ���� � ������ ������
		for (String action : this.actions) {
			if (!action.substring(0, 4).matches("P.S.")) {
				/*
				 * ��������� ����� XWPFParagraph , �� � �������� ������. ������� ����� ���
				 * ������
				 */
				XWPFParagraph paragraph2 = document.createParagraph();
				paragraph2.setAlignment(ParagraphAlignment.LEFT);
				paragraph2.setSpacingAfter(0);
				/*
				 * ��������� ����� XWPFRun , �� � �������� ���� ��� ������. ������� ����� ���
				 * ����
				 */
				XWPFRun run5 = paragraph2.createRun();
				run5.setFontSize(15);
				//����� ��䳿 � �� �����
				if (!action.substring(0, 2).matches("[0-9]."))
					run5.setText(count + ". " + action);
				else
					run5.setText(action);
				run5.addBreak();
				count++;
			}
		}
		for (String action : this.actions) {
			if (action.substring(0, 4).matches("P.S.")) {
				/*
				 * ��������� ����� XWPFParagraph , �� � �������� ������. ������� ����� ���
				 * ������
				 */
				XWPFParagraph paragraph2 = document.createParagraph();
				paragraph2.setAlignment(ParagraphAlignment.LEFT);
				paragraph2.setSpacingAfter(0);
				/*
				 * ��������� ����� XWPFRun , �� � �������� ���� ��� ������. ������� ����� ���
				 * ����
				 */
				XWPFRun run5 = paragraph2.createRun();
				run5.setFontSize(15);
				run5.setText(action);
				run5.addBreak();
			}
		}
		// ����� �������� �����������
		writeFooter(document, name);
	}

	// ���������������� ������ ��� ������ �������� �����������
	@Override
	public void writeFooter(XWPFDocument document, String name) {
		/*
		 * ��������� ����� XWPFParagraph , �� � �������� ������. ������� ����� ���
		 * ������
		 */
		XWPFParagraph paragraph1 = document.createParagraph();
		paragraph1.setAlignment(ParagraphAlignment.RIGHT);
		paragraph1.setSpacingAfter(0);
		paragraph1.setBorderBottom(Borders.BASIC_THIN_LINES);
		/*
		 * ��������� ����� XWPFRun , �� � �������� ���� ��� ������. ������� ����� ���
		 * ����
		 */
		XWPFRun run1 = paragraph1.createRun();
		run1.setFontSize(15);
		run1.setItalic(true);
		// ����� ����������� ��� ��������
		run1.setText("�������� � " + protocolNumber + " �� " + printDocumentDate.getDayOfMonth() + "."
				+ printDocumentDate.getMonthValue() + "." + printDocumentDate.getYear() + "�.");
		// ������ ������ � ����
		try {
			FileOutputStream output = new FileOutputStream(name + ".docx");
			document.write(output);
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	};

	// ����� ��� ����������� � �����
	public String readFromWord(File file) throws FileNotFoundException, IOException {
		String[] documentParts = null;
		/*
		 * � ������� ��������� ����� ��������� ������������ ��������� � ���������
		 * ���������� � ����� ��������� ����������
		 */
		if (file.exists()) {
			XWPFDocument document = new XWPFDocument(new FileInputStream(file));
			XWPFWordExtractor extract = new XWPFWordExtractor(document);
			documentParts = extract.getText().split("������|������|��������");
			extract.close();
			return documentParts[1];
		}
		return null;
	}

}
