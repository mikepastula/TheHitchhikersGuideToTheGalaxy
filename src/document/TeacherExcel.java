package document;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import model.Lesson;

public class TeacherExcel {
	private Workbook workbook;
	private String path;
	int id;

	/*
	 * ������� ����������� ���� � ��������� ���������.
	 */
	public void setFile(String name) {
		path = name;
		// � ��������� �� ���������� ����� ���������� ���� ������� � �����������
		// ������
		try {
			FileInputStream stream = null;
			if (name.endsWith(".xlsx")) {
				stream = new FileInputStream(name);
				workbook = new XSSFWorkbook(stream);
			}
			if (name.endsWith(".xls")) {
				stream = new FileInputStream(name);
				workbook = new HSSFWorkbook(stream);
			}
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * �������� ��������� ��������� � �������� ��'�� � ��������. ������� -1 �
	 * ������� ���� ���������� �� ��� ���������.
	 */
	public int findTeacher(String nameTeacher) {
		// ³������� ������������ ����� � ���������
		Sheet sheet = workbook.getSheetAt(0);
		int i = 7;
		boolean f = true;
		// ����� � �������� �� ��������� ���� ��������� ������
		for (; i < sheet.getLastRowNum() && f; ++i) {
			String rez = sheet.getRow(i).getCell(0).getStringCellValue().replaceAll("\n", " ");
			if (rez.equals(nameTeacher))
				f = false;
		}
		id = !f ? i - 1 : -1;
		return id;
	}

	// ������� �����, � ����� ����������� ���� ��� ���������.
	public int getId() {
		return id;
	}

	/*
	 * ������� ������ ���, �� ��������� ��������.
	 */
	public List<Lesson> getLessons(int row) {
		// ��������� ������ ���
		List<Lesson> lessons = new ArrayList<>();
		// ��������� ����������� �����, � ���� ���� ���������� ����������� ������.
		Row r = workbook.getSheetAt(0).getRow(row);
		Row r1 = workbook.getSheetAt(0).getRow(row + 1);
		Row r2 = workbook.getSheetAt(0).getRow(row + 2);
		// ������ �� �������� �������� � �������� � ����� ������.
		for (int i = 1; i < r.getLastCellNum(); ++i) {
			// � ��������� �� ����������� ����� ���������� ���������� ������.
			if (!r.getCell(i).getStringCellValue().isEmpty() && r1.getCell(i).getStringCellValue().isEmpty()) {
				// ����������� ������ � ������.
				String temp = workbook.getSheetAt(0).getRow(row + 3).getCell(i).getStringCellValue();
				// ��������� ���� �� ����� ��������� � ������ � ��������� ���� �� ������ ���.
				Lesson tempL = new Lesson();
				tempL.setAud(temp);
				try {
					tempL.setGroup(workbook.getSheetAt(0).getRow(row + 2).getCell(i).getStringCellValue());
				} catch (Exception e) {
					tempL.setGroup(
							Double.toString(workbook.getSheetAt(0).getRow(row + 2).getCell(i).getNumericCellValue()));
				}
				tempL.setLessonName(workbook.getSheetAt(0).getRow(row).getCell(i).getStringCellValue());
				tempL.setLesson(workbook.getSheetAt(0).getRow(5).getCell(i).getStringCellValue());
				int tempCell = i;
				while (workbook.getSheetAt(0).getRow(3).getCell(tempCell).getStringCellValue().equals(""))
					tempCell--;
				tempL.setDay(
						workbook.getSheetAt(0).getRow(3).getCell(tempCell).getStringCellValue().replaceAll(" ", ""));
				tempL.setType("����� �������");
				lessons.add(tempL);
			} else {
				if (!r.getCell(i).getStringCellValue().isEmpty() && !r1.getCell(i).getStringCellValue().isEmpty()) {
					// ��������� ���� �� ����� ��������� � ������ � ��������� ���� �� ������ ���.
					Lesson tempL1 = new Lesson();
					tempL1.setLessonName(workbook.getSheetAt(0).getRow(row).getCell(i).getStringCellValue());
					String[] t1 = workbook.getSheetAt(0).getRow(row + 1).getCell(i).getStringCellValue().split(",");
					String tempS = "";
					for (String s : t1) {
						s.replaceAll(" ", "");
						if (!s.matches("^\\w*"))
							tempL1.setAud(s);
						else {
							tempS += "," + s;
						}
					}
					tempL1.setGroup(tempS);
					tempL1.setLesson(workbook.getSheetAt(0).getRow(5).getCell(i).getStringCellValue());
					int tempCell = i;
					while (workbook.getSheetAt(0).getRow(3).getCell(tempCell).getStringCellValue().equals(""))
						tempCell--;
					tempL1.setDay(workbook.getSheetAt(0).getRow(3).getCell(tempCell).getStringCellValue()
							.replaceAll(" ", ""));
					tempL1.setType("������ �������");
					lessons.add(tempL1);
				} else if (!r2.getCell(i).getStringCellValue().isEmpty()) {
					// ��������� ���� �� ����� ��������� � ������ � ��������� ���� �� ������
					// ���.�
					Lesson tempL2 = new Lesson();
					tempL2.setLessonName(workbook.getSheetAt(0).getRow(row + 2).getCell(i).getStringCellValue());

					String[] t2 = workbook.getSheetAt(0).getRow(row + 3).getCell(i).getStringCellValue().split(",");
					String tempS = "";
					for (String s : t2) {
						s.replaceAll(" ", "");
						if (!s.matches("^\\w*"))
							tempL2.setAud(s);
						else {
							tempS += "," + s;
						}
					}
					tempL2.setGroup(tempS.substring(1));
					tempL2.setLesson(workbook.getSheetAt(0).getRow(5).getCell(i).getStringCellValue());
					int tempCell = i;
					while (workbook.getSheetAt(0).getRow(3).getCell(tempCell).getStringCellValue().equals(""))
						tempCell--;
					tempL2.setDay(workbook.getSheetAt(0).getRow(3).getCell(tempCell).getStringCellValue()
							.replaceAll(" ", ""));
					tempL2.setType("������ �������");
					lessons.add(tempL2);
				}
			}
		}
		return lessons;
	}

	/*
	 * ��������� ���� � ��������. �� ���� � ��� ���������� �� ���� �������� �
	 * ������������ � ������� �� ������� ��������������� ����
	 */

	public void DeleteLesson(Lesson l, int row) {
		l.setGroup("");
		l.setLessonName("");
		l.setAud("");
		this.SetLesson(l, row);
	}

	/*
	 * ��������� ���� � �������
	 */

	public void UpdateLesson(Lesson l, Lesson l2, int row) {
		// ��������� ������ ��������� ��� ���� � �������
		DeleteLesson(l2, row);
		// ��������� ����� ��������� ��� ����
		SetLesson(l, row);
	}

	/*
	 * ����� ���� � �������
	 */
	public void SetLesson(Lesson l, int row) {
		// ��������� ����������� �����, � �� ���� ���������� ����� ����������.
		Row r = workbook.getSheetAt(0).getRow(3);
		Row r1 = workbook.getSheetAt(0).getRow(5);
		int i = 0;
		// ������������� ���� �������� � �������.
		while (!l.getDay().equals(r.getCell(i).getStringCellValue().replaceAll(" ", ""))) {
			i++;
		}
		while (!l.getLesson().equals(r1.getCell(i).getStringCellValue())) {
			i++;
		}

		// ��������� ����� ��� ���������, �� ���� �������� � ����.
		CellStyle style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		Font font = workbook.createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 16);
		style.setFont(font);

		// � ��������� �� ���������� ���� ���������� ��������� ������ � �������� �
		// �������� �����
		switch (l.getType()) {
		case "������ �������":
			// ��������� ���������� ������ � ������� � ������� �� ������������
			Cell cell = workbook.getSheetAt(0).getRow(row).createCell(i);
			cell.setCellStyle(style);
			//��������� ���������� ��� ���� � ������ �������
			cell.setCellValue(l.getLessonName());
			cell = workbook.getSheetAt(0).getRow(row + 1).createCell(i);
			cell.setCellStyle(style);
			//��������� ���������� ��� ���� � ������ �������
			cell.setCellValue(l.getGroup() + "," + l.getAud());
			if (workbook.getSheetAt(0).getRow(row + 3).getCell(i).getStringCellValue().startsWith(" I -")) {
				cell = workbook.getSheetAt(0).getRow(row + 3).createCell(i);
				cell.setCellValue("");
			}
			break;
		case "������ �������":
			// ��������� ���������� ������ � ������� � ������� �� ������������
			Cell cell1 = workbook.getSheetAt(0).getRow(row + 2).createCell(i);
			cell1.setCellStyle(style);
			//��������� ���������� ��� ���� � ������ �������
			cell1.setCellValue(l.getLessonName());
			cell1 = workbook.getSheetAt(0).getRow(row + 3).createCell(i);
			cell1.setCellStyle(style);
			//��������� ���������� ��� ���� � ������ �������
			cell1.setCellValue(l.getGroup() + "," + l.getAud());
			if (workbook.getSheetAt(0).getRow(row + 1).getCell(i).getStringCellValue().contains("I -")) {
				cell = workbook.getSheetAt(0).getRow(row + 1).createCell(i);
				cell.setCellValue("");
				cell = workbook.getSheetAt(0).getRow(row).createCell(i);
				cell.setCellValue("");
			}
			break;
		default:
			// ��������� ���������� ������ � ������� � ������� �� ������������
			Cell cell2 = workbook.getSheetAt(0).getRow(row).createCell(i);
			cell2.setCellStyle(style);
			//��������� ���������� ��� ���� � ������ �������
			cell2.setCellValue(l.getLessonName());
			cell2 = workbook.getSheetAt(0).getRow(row + 2).createCell(i);
			cell2.setCellStyle(style);
			//��������� ���������� ��� ���� � ������ �������
			cell2.setCellValue(l.getGroup());
			cell2 = workbook.getSheetAt(0).getRow(row + 3).createCell(i);
			cell2.setCellStyle(style);
			//��������� ���������� ��� ���� � ������ �������
			cell2.setCellValue(l.getAud());
			if (!workbook.getSheetAt(0).getRow(row + 1).getCell(i).getStringCellValue().equals("")) {
				cell = workbook.getSheetAt(0).getRow(row + 1).createCell(i);
				cell.setCellValue("");
			}
		}
		System.out.println("setted");
		//������ ������ ���������� � ������������ ��������� � ���������
		try {
			FileOutputStream output = new FileOutputStream(path);
			workbook.write(output);
			output.close();
			System.out.println("true");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
