package view;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import application.Main;
import document.TeacherExcel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.DriveUpDown;
import model.Lesson;
import model.User;

public class TeacherController implements Initializable {
	@FXML
	private TableView<Lesson> lessonsTable;
	@FXML
	private TableColumn<Lesson, String> dayColumn;
	@FXML
	private TableColumn<Lesson, String> lessonColumn;
	@FXML
	private TableColumn<Lesson, String> nameColumn;
	@FXML
	private TableColumn<Lesson, String> groupsColumn;
	@FXML
	private TableColumn<Lesson, String> aud;
	@FXML
	private TableColumn<Lesson, String> weekColumn;

	@FXML
	private TextField name;
	@FXML
	private TextField group;
	@FXML
	private TextField auditory;
	@FXML
	private ChoiceBox<String> day = new ChoiceBox<>();
	@FXML
	private ChoiceBox<String> numLesson = new ChoiceBox<>();
	@FXML
	private ChoiceBox<String> week = new ChoiceBox<>();

	private User person;
	// ��������� �� �������� ����-��������� � �������.
	private Main mainApp;
	// ��������� �� ����������� ���� ��� ������� ��������.
	private TeacherExcel t = new TeacherExcel();

	public TeacherController() {
	}

	// ���������� ���� ���������� ��� ���� ���������.
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// ���������� ������ ������ ��� ��������� ���� ���� � �������.
		ObservableList<String> weeks //
				= FXCollections.observableArrayList("����� �������", "������ �������", "������ �������");
		week.setItems(weeks);
		ObservableList<String> days//
				= FXCollections.observableArrayList("��������", "�������", "������", "�������", "�������");
		day.setItems(days);
		ObservableList<String> lessons //
				= FXCollections.observableArrayList("1  ����", "2  ����", "3  ����", "4  ����", "5  ����", "6  ����");
		numLesson.setItems(lessons);

		// ���������� ������� � ���������� ��� ���� ��������� ��� ������� �����.
		dayColumn.setCellValueFactory(cellData -> cellData.getValue().getDayProperty());
		lessonColumn.setCellValueFactory(cellData -> cellData.getValue().getLessonProperty());
		nameColumn.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		aud.setCellValueFactory(cellData -> cellData.getValue().getAudProperty());
		groupsColumn.setCellValueFactory(cellData -> cellData.getValue().getGroupProperty());
		weekColumn.setCellValueFactory(cellData -> cellData.getValue().getTypeProperty());

		showLessonDetails(null);

		lessonsTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> showLessonDetails(newValue));

	}

	/*
	 * ���� �������� ���������� ��� ������ ����
	 */
	private void showLessonDetails(Lesson lesson) {
		if (lesson != null) {
			day.setValue(lesson.getDay());
			week.setValue(lesson.getType());
			numLesson.setValue(lesson.getLesson());
			auditory.setText(lesson.getAud());
			name.setText(lesson.getLessonName());
			group.setText(lesson.getGroup());
		} else {
			day.setValue("��������");
			week.setValue("����� �������");
			numLesson.setValue("1 ����");
			auditory.setText(" ");
			name.setText("");
			group.setText("");
		}
	}

	/*
	 * ��������� ���� � ��������.
	 */
	@FXML
	private void handleDeleteLesson() throws Exception {
		// ��������� ������ � ������ ������� ����.
		int selectedIndex = lessonsTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			// ��������� ��������� ��� ������� ���� � ��������� �� � ��������.
			Lesson temp = lessonsTable.getItems().get(selectedIndex);
			System.out.println(temp);
			t.DeleteLesson(temp, t.getId());
			temp.setDay("");
			temp.setLesson("");
			temp.setType("");
			System.out.println("set null");
			// ��������� ������ � ���� � ������� � ������� � ����.
			new DriveUpDown().updateFile("application/vnd.ms-excel", "Rozklad_CHNU_FPM.xlsx");
			this.lessonsTable.refresh();
			this.setMainApp(this.mainApp);
			this.lessonsTable.refresh();
		} else {
			System.out.println("ERROR");
		}
	}

	/*
	 * ���� ���������� ��� ����.
	 */
	@FXML
	private void handleEditLesson() throws Exception {
		// ��������� ������ � ������ ������� ����.
		int selectedIndex = lessonsTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			// ��������� ��������� ��� ������� ����
			Lesson temp2 = lessonsTable.getItems().get(selectedIndex);
			Lesson temp = new Lesson();
			// �������� ���������� �������� ����� � ��������� ��� � �������.
			if (!auditory.getText().isEmpty() && !group.getText().isEmpty() && !name.getText().isEmpty()) {
				temp.setAud(auditory.getText());
				temp.setDay(day.getValue());
				temp.setGroup(group.getText());
				temp.setLesson(numLesson.getValue());
				temp.setLessonName(name.getText());
				temp.setType(week.getValue());
				setMainApp(this.mainApp);
			} else {
				System.out.println("ERROR");
				return;
			}
			// ��������� ������ � ���� � ������� � ������� � ����.
			t.UpdateLesson(temp, temp2, t.getId());
			new DriveUpDown().updateFile("application/vnd.ms-excel", "Rozklad_CHNU_FPM.xlsx");
			this.lessonsTable.refresh();
			this.setMainApp(this.mainApp);
			this.lessonsTable.refresh();
		} else {
			System.out.println("ERROR");
		}
	}

	/*
	 * ��������� ��������� ��� ���� ���� � �������.
	 */
	@FXML
	private void handleNewLesson() throws Exception {
		/*
		 * ��������� ���� � �������� �������� ������� �� �����������. � �������
		 * ��������� �������� ����� ���������� ��������� ��� � ���� � �� ����.
		 */
		Lesson temp = new Lesson();
		if (!auditory.getText().isEmpty() && !group.getText().isEmpty() && !name.getText().isEmpty()) {
			temp.setAud(auditory.getText());
			temp.setDay(day.getValue());
			temp.setGroup(group.getText());
			temp.setLesson(numLesson.getValue());
			temp.setLessonName(name.getText());
			temp.setType(week.getValue());
			t.SetLesson(temp, t.getId());
			new DriveUpDown().updateFile("application/vnd.ms-excel", "Rozklad_CHNU_FPM.xlsx");
			setMainApp(this.mainApp);
		} else {
			System.out.println("ERROR");
		}
	}
	
	//�������� ������� �����, ���������� �� ����� �����������.
	@FXML
	private void ExitAction(ActionEvent event) throws IOException {
		(((Node) event.getSource()).getScene()).getWindow().hide();
		mainApp.initRootLayout();

	}
	
	//��������� ��������� �� �������� ����-����������.
	public void setMainApp(Main main) throws Exception {
		this.mainApp = main;
		this.person = main.getCurrentUser();
		//������ ������������ ����� � ��������� � �����
		try {
			new DriveUpDown().downloadFile("Rozklad_CHNU_FPM.xlsx", "application/vnd.ms-excel");
		} catch (Exception e) {
			e.printStackTrace();
			mainApp.showDownloadLessonsError();
			return;
		}
		//����� ��������� � ����� � ����������� �������� �� �������� ��������� ����������� � ������.
		t.setFile(System.getProperty("user.home") + "/chnuPM/Rozklad_CHNU_FPM.xlsx");
		String name = person.getFirstnameDecode() + " " + person.getLastnameDecode() + " "
				+ person.getSecondnameDecode();
		if (t.findTeacher(name) > 0) {
			List<Lesson> lessons = t.getLessons(t.findTeacher(name));
			if (!lessons.isEmpty()) {
				ObservableList<Lesson> tempLessons = FXCollections.observableArrayList(lessons);
				this.lessonsTable.setItems(tempLessons);
			}
		} else {
			throw new Exception();
		}
	}

}
