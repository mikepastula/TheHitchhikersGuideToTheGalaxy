package view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.User;

public class TeacherOverviewController implements Initializable {
	@FXML
	private TableView<User> personTable;
	@FXML
	private TableColumn<User, String> firstNameColumn;
	@FXML
	private TableColumn<User, String> lastNameColumn;
	@FXML
	private TextField firstName;
	@FXML
	private TextField lastName;
	@FXML
	private TextField secondName;
	@FXML
	private TextField email;
	@FXML
	private PasswordField password;
	@FXML
	private CheckBox sendIt;
	// ��������� �� �������� ����-��������� � �������.
	private Main mainApp;

	public TeacherOverviewController() {
	}

	//���������� ����� ������� ��� ������.
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//���������� �������� ���������� ��� ������������.
		firstNameColumn
				.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirstnameDecode()));
		lastNameColumn
				.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLastnameDecode()));

		//�������� ���� ���������� ��������� �����������.
		showPersonDetails(null);

		//��������������� ��� � ������� � ����������� ��������� ��� �������� �����������.
		personTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> showPersonDetails(newValue));
	}

	/*
	 * �������� ������� ���� ���������� ��� ����������� ��� ��������� � ����.
	 */
	private void showPersonDetails(User person) {
		// ���� ������� ���������� �� �������� � ���� ���������� ���� ���������� �
		// ������� ����.
		if (person != null && person.getRole().equals("user")) {
			firstName.setText(person.getFirstnameDecode());
			lastName.setText(person.getLastnameDecode());
			secondName.setText(person.getSecondnameDecode());
			email.setText(person.getEmailDecode());
			password.setText(person.getPasswordDecode());
			sendIt.setSelected(person.getSend());
		} else {
			//� ����� �������� ���� ��������� ������� ����������.
			firstName.setText("");
			lastName.setText("");
			secondName.setText("");
			email.setText("");
			password.setText("");
			sendIt.setSelected(false);
		}
	}

	/*
	 * ��������� ����������� � �������.
	 */
	@FXML
	private void handleDeletePerson() {
		// ��������� ������ � ������ ��������� �����������.
		int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			/*
			 * � ������� ���� �� �� �������� ���������� ��������� � ������ ������������ �
			 * ����-��������� � ��������� ������ � ���� � �������.
			 */
			User temp = personTable.getItems().get(selectedIndex);
			if (temp.getRole().equals("user")) {
				mainApp.deleteUser(selectedIndex);
				mainApp.savePersonDataToFile();
				ObservableList<User> tempUsers = FXCollections.observableArrayList(mainApp.getUsers());
				this.personTable.setItems(tempUsers);
			}
		} else {
			System.out.println("ERROR");
		}
	}

	/*
	 * ����� ��� ��������� ������ �����������.
	 */
	@FXML
	private void handleNewPerson() {
		User tempPerson = new User();
		// ��������� ��������� ��� ������ ��������� � �������� ������ �� �����������.
		if (firstName.getText().matches("^[\\D]+$") && lastName.getText().matches("^[\\D]+$")
				&& secondName.getText().matches("^[\\D]+$") && email.getText().matches("^[a-zA-Z0-9]+@[a-z]+[.][a-z]+$")
				&& !password.getText().equals("")) {
			/*
			 * � ������� ��������� ������ ���������� ��������� ��������� � �������. �
			 * ��������� ������ � ������� � �����������
			 */
			tempPerson.setFirstnameCode(firstName.getText());
			tempPerson.setLastnameCode(lastName.getText());
			tempPerson.setEmailCode(email.getText());
			tempPerson.setSecondnameCode(secondName.getText());
			tempPerson.setPasswordCode(password.getText());
			tempPerson.setRole("user");
			tempPerson.setSend(true);
			mainApp.addUser(tempPerson);
			ObservableList<User> tempUsers = FXCollections.observableArrayList(mainApp.getUsers());
			this.personTable.setItems(tempUsers);
		} else {
			System.out.println(firstName.getText().matches("^[\\D]+$"));
			System.out.println(lastName.getText().matches("^[\\D]+$"));
			System.out.println(secondName.getText().matches("^[\\D]+$"));
			System.out.println(email.getText().matches("^[a-zA-Z0-9]+@[a-z]+[.][a-z]+$"));
			System.out.println(!password.getText().equals(""));
		}
	}

	// �������� ������� �����.
	@FXML
	private void GetBackAction(ActionEvent event) throws IOException {
		(((Node) event.getSource()).getScene()).getWindow().hide();
	}

	/*
	 * �������� ������� ���������� ��� ����������� � ������
	 */
	@FXML
	private void handleSave() {
		// ��������� ������ � ������ ��������� ���������
		int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
		User temp = mainApp.getUsers().get(selectedIndex);
		// ��������� ����� ��������� ��� ��������� � �������� ������ �� �����������
		if (firstName.getText().matches("^[\\D]+$") && lastName.getText().matches("^[\\D]+$")
				&& email.getText().matches("^[a-zA-Z0-9]+@[a-z]+[.][a-z]+$") && !password.getText().equals("")
				&& temp.getRole().equals("user")) {
			// � ������� ��������� ������ ���������� ����� ����������
			temp.setFirstnameCode(firstName.getText());
			temp.setLastnameCode(lastName.getText());
			temp.setEmailCode(email.getText());
			temp.setSecondnameCode(secondName.getText());
			temp.setPasswordCode(password.getText());
			temp.setRole("user");
			temp.setSend(sendIt.isSelected());
			mainApp.deleteUser(selectedIndex);
			mainApp.addUser(temp);
		} else {
			System.out.println(firstName.getText().matches("^[\\D]+$"));
			System.out.println(lastName.getText().matches("^[\\D]+$"));
			System.out.println(secondName.getText().matches("^[\\D]+$"));
			System.out.println(email.getText().matches("^[a-zA-Z0-9]+@[a-z]+[.][a-z]+$"));
			System.out.println(!password.getText().equals(""));
		}
	}

	/*
	 * ��������� ��������� �� �������� ����-���������� � ���������� ������ � �������
	 * ���������� �� �������� � ���� ���������.
	 */
	public void setMainApp(Main main) {
		this.mainApp = main;
		ObservableList<User> tempUsers = FXCollections.observableArrayList(main.getUsers());
		this.personTable.setItems(tempUsers);
	}
}
