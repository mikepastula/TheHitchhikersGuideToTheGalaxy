package view;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.DriveUpDown;
import model.User;

public class LoginController implements Initializable {
	@FXML
	private TextField login;
	@FXML
	private PasswordField password;
	@FXML
	private Label message;
	@FXML
	private List<User> users;

	// ��������� �� �������� ����-��������� � �������.
	private Main mainApp;

	public LoginController() {
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	/*
	 * �������� ��������� ����������� � ������.
	 */
	@FXML
	private void loginAction(ActionEvent event) throws Exception {
		// ���������� ����� � ������ �����������. �������� ��������� ����� ������ �
		// ������.
		for (User user : users) {
			if (login.getText().equals(user.getEmailDecode())) {
				if (password.getText().equals(user.getPasswordDecode())) {
					(((Node) event.getSource()).getScene()).getWindow().hide();
					/*
					 * ���� ���������� ����, � ���� ���� ��������,����������� ����� ���������, 
					 * ������ ����������� ������� ���������.
					 * ��� ������� ����� ���������� �������� �� ����������� �������� ����� ����������.
					 */
					if (user.getRole().equals("user")) {
						mainApp.setCurrentUser(user);
						try {
							if (!new DriveUpDown().checkIsModified("Rozklad_CHNU_FPM.xlsx", "application/vnd.ms-excel"))
								mainApp.showTeacher();
						} catch (Exception e) {
							mainApp.showDownloadLessonsError();
						}
					} else {
						mainApp.setCurrentUser(user);
						mainApp.showEmail();
					}
				} else {
					message.setText("������������ ������");
				}
			} else {
				message.setText("������������ ����");
			}
		}
	}
	
	//����� ��� ��������� �� ������� �����.
	@FXML
	private void GetBackAction(ActionEvent event) throws IOException {
		//�������� ������� ����� � �������� ����� �����������.
		(((Node) event.getSource()).getScene()).getWindow().hide();
		Parent parent = FXMLLoader.load(getClass().getResource("/controller/Login.fxml"));
		Scene scene = new Scene(parent);
		Stage stage = new Stage();
		stage.setTitle("�����������");
		stage.setScene(scene);
		stage.show();
	}
	
	//�������� ������� ����� � ����� � ��������.
	@FXML
	private void Hide(ActionEvent event) {
		(((Node) event.getSource()).getScene()).getWindow().hide();
		this.mainApp.close();
	}
	
	//�������� ������� �����.
	@FXML
	private void JustHide(ActionEvent event) {
		(((Node) event.getSource()).getScene()).getWindow().hide();
	}
	
	/*
	 * ��������� ��������� �� �������� ����-����������.
	 * ��������� ������ ��� ��� ������������ � ������.
	 */
	public void setMainApp(Main main) {
		this.mainApp = main;
		this.users = main.getUsers();
	}

}
