package application;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.DriveUpDown;
import model.User;
import model.UserSerializer;
import view.AdvertisementController;
import view.EmailSendController;
import view.FileOverViewController;
import view.LoginController;
import view.TeacherController;
import view.TeacherOverviewController;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class Main extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	// ������ � �������, ����� ����� �������
	private List<File> files = new ArrayList<>();
	private List<User> personData = new ArrayList<>();
	private User current = new User();

	// ����������� ��������� �����
	public Main() {
	}

	public static void main(String[] args) {
		launch(args);
	}

	/*
	 * ����� �� ���������� ��� ������� ��������. �������� �������� ���������
	 * ���������� ��� ������� ������������ � ������ ���� ������� ���������� 0, ��
	 * ����������� 1 ���������� - ����. ��� ����������� �����, �� ������� ��
	 * �������� ����� ����������. � ��� ���������� ������� ����������� ����� �
	 * ������������ ��� �������.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			/*
			 * ��������� ��'����,�� �������� ��'�������� �� ����� � ������ ������������
			 * ���������� ��� ������������.
			 */
			DriveUpDown d = new DriveUpDown();
			d.downloadFile("users.json", "application/json");
			/*
			 * ³������� ����� � ����������� ��� ������������ ������ ����������� � �����.
			 */
			File file = new File(System.getProperty("user.home") + "/chnuPM/users.json");
			if (file.exists()) {
				loadPersonDataFromFile(file);
			}

			for (User user : personData) {
				if (user.getRole().equals("user"))
					user.setSend(true);
			}
			if (personData.size() == 0) {
				User u = new User();
				u.setFirstnameCode("Admin");
				u.setLastnameCode("Admin");
				u.setPasswordCode("1");
				u.setEmailCode("1");
				u.setRole("admin");
				u.setSend(false);
				u.setSecondnameCode("Admin");
				personData.add(u);
				savePersonDataToFile();
			}
			// ������� ������������ ��� ����� � ������ ������ �� �� �������.
			this.primaryStage = primaryStage;
			this.primaryStage.getIcons().add(new Image("/resources/image.png"));
			this.primaryStage.setTitle("��������� ������ �� ������ ��������� ����������");
			initRootLayout();
		} catch (Exception e) {
			/*
			 * ���� � ������� ���������� ��� ������� � ������ ������, �� ������ ����� �
			 * ���'�������� ���� ����� �������.
			 */
			e.printStackTrace();
			this.primaryStage = primaryStage;
			this.primaryStage.getIcons().add(new Image("file:resources/image.png"));
			this.primaryStage.setTitle("������� ������������");
			showDownloadUsersError();
		}
	}

	/*
	 * ³������� ����� �����������.
	 */
	public void initRootLayout() {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/Login.fxml"));
			rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			LoginController controller = loader.getController();
			controller.setMainApp(this);
			primaryStage.getIcons().add(new Image("/resources/image.png"));
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * ³������� ����� � ���������� ���������� ������� ��� ������� �������� � ������
	 * ��������� ������ ��� ������������.
	 */
	public void showDownloadUsersError() {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/ErrorJson.fxml"));
			rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			LoginController controller = loader.getController();
			controller.setMainApp(this);
			primaryStage.getIcons().add(new Image("/resources/image.png"));
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * ³������� ���� ����������� ��� ������ ��������� ����������
	 */
	public void showGoodFile() {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/WordCreate.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			// ������� ������������ ��� �����
			dialogStage.setTitle("���������� ���� ��������");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("/resources/image.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			LoginController controller = loader.getController();
			System.out.println(controller == null);
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/*
	 * ³������� ����� ���������
	 */
	public void showEmail() {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/Email.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			// ������� ������������ ��� �����
			dialogStage.setOnHidden(e -> Platform.exit());
			dialogStage.setTitle("³�������� ����������� ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("/resources/image.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			EmailSendController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
			showDownloadUsersError();
		}
	}

	/*
	 * ³������� ����� ���������
	 */
	public void showTeacher() {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/Teacher.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			// ������� ������������ ��� �����
			dialogStage.setTitle("������� ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("/resources/image.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			TeacherController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (Exception e) {
			e.printStackTrace();
			showDownloadLessonsError();
		}
	}

	/*
	 * ³������� ���� � ������������ ��� ������ ����������� ����� ���������
	 */
	public void showOk() {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/Ok.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			// ������� ������������ ��� �����
			dialogStage.setTitle("³���������");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("/resources/image.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
			showDownloadLessonsError();
		}
	}

	/*
	 * ³������� ����� � ������� ���� ������� ���������� ���������� ������ �������
	 * ����������.
	 */
	public void showDownloadLessonsError() {
		// ³������� ����� �����������
		initRootLayout();
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/DownloadFile.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			// ������� ������������ ��� �����
			dialogStage.setTitle("������� ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("/resources/image.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			LoginController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * ³������� ����� ��������� ��������� �������� �� ��������� ����
	 */
	public void showAdvertisement(String name) {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource(name));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			// ������� ������������ ��� �����
			dialogStage.setTitle("����������");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("/resources/image.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			AdvertisementController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
			showDownloadUsersError();
		}
	}

	/*
	 * ³������� ����� ��������� ������������ ������� � ������
	 */
	public void showTeacherOverview() {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/TeacherOverview.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			// ������� ������������ ��� �����
			dialogStage.setTitle("���������");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("/resources/image.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			TeacherOverviewController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
			showDownloadUsersError();
		}
	}

	/*
	 * ³������� ����� ��������� �����, ����������� �� �����������
	 */
	public void showFilesOverview() {
		try {
			// ��������� ������������� �������
			FXMLLoader loader = new FXMLLoader();
			// ������������ �����
			loader.setLocation(Main.class.getResource("/view/FilesOverview.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			// ������� ������������ ��� �����
			dialogStage.setTitle("�����");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.getIcons().add(new Image("/resources/image.png"));
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			/*
			 * ��������� �����-����������, �� ������� �� ������� ���� ��������� �
			 * ������. ��������� � ����-���������� ��������� �� ������� ���� �������.
			 */
			FileOverViewController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
			showDownloadUsersError();
		}
	}

	/*
	 * ������� ����� , � ����� ������� �����������. ��� ������� ����������� ����
	 * ���������� - ��������
	 */
	public void loadPersonDataFromFile(File file) {
		personData.clear();
		try {
			personData = new UserSerializer().deserialize(file);
		} catch (Exception e) {
			e.printStackTrace();
			User u = new User();
			u.setFirstnameCode("Admin");
			u.setLastnameCode("Admin");
			u.setPasswordCode("1");
			u.setEmailCode("1");
			u.setRole("admin");
			u.setSend(false);
			u.setSecondnameCode("Admin");
			personData.add(u);
			savePersonDataToFile();
		}
	}

	/*
	 * ���������� ������ ��� ������������ � ���� � ��������� ���������� � ����
	 */
	public void savePersonDataToFile() {
		try {
			File file = new File(System.getProperty("user.home") + "/chnuPM/users.json");
			new UserSerializer().serialize(personData, file);
			new DriveUpDown().updateFile("application/json", "users.json");
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
			showDownloadUsersError();
		}
	}

	/*
	 * ����� �������� ��������� ��� ������������ ������� � ������
	 */
	public List<User> getUsers() {
		return personData;
	}

	/*
	 * ����� �������� ������ ������ �����������
	 */
	public void addUser(User user) {
		personData.add(user);
		savePersonDataToFile();
	}

	/*
	 * ����� �������� ��������� �� ����� ���������� �� �����
	 */
	public List<File> getFiles() {
		return files;
	}

	/*
	 * ����� �������� ������ ����� ����
	 */
	public void addFile(File file) {
		files.add(file);
	}

	/*
	 * ����� ��� ��������� ���������� ��� ��������� �����������
	 */
	public void setCurrentUser(User user) {
		this.current = user;
	}

	/*
	 * ����� ��� ���������� ���������� ��� ��������� �����������
	 */
	public User getCurrentUser() {
		return current;
	}

	/*
	 * ����� ��� ��������� ����������� � �������
	 */
	public void deleteUser(int id) {
		personData.remove(id);
		savePersonDataToFile();
	}

	/*
	 * ����� ��� ��������� ����� � �����
	 */
	public void deleteFile(int id) {
		files.remove(id);
	}

	/*
	 * ����� ��� ������ � ��������
	 */
	public void close() {
		Platform.exit();
	}
}
