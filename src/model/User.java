package model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Base64;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class User {
	private StringProperty firstname;
	private StringProperty lastname;
	private StringProperty secondname;
	private StringProperty role;
	private StringProperty email;
	private StringProperty password;
	private BooleanProperty send;
	
	
	public void setFirstnameCode(String firstname) {
		this.firstname = new SimpleStringProperty(Base64.getEncoder().encodeToString(firstname.getBytes()));
	}
	
	public void setLastnameCode(String lastname) {
		this.lastname = new SimpleStringProperty(Base64.getEncoder().encodeToString(lastname.getBytes()));
	}
	
	public void setSecondnameCode(String secondname) {
		this.secondname = new SimpleStringProperty(Base64.getEncoder().encodeToString(secondname.getBytes()));
	}
	
	public void setPasswordCode(String password) {
		this.password = new SimpleStringProperty(Base64.getEncoder().encodeToString(password.getBytes()));
	}
	
	public void setEmailCode(String email) {
		this.email = new SimpleStringProperty(Base64.getEncoder().encodeToString(email.getBytes()));
	}
	@JsonIgnore
	public String getFirstnameDecode() {
		return new String(Base64.getDecoder().decode(firstname.get()));
	}
	@JsonIgnore
	public String getSecondnameDecode() {
		return new String(Base64.getDecoder().decode(secondname.get()));
	}
	@JsonIgnore
	public String getLastnameDecode() {
		return  new String(Base64.getDecoder().decode(lastname.get()));
	}
	@JsonIgnore
	public String getEmailDecode() {
		return  new String(Base64.getDecoder().decode(email.get()));
	}
	@JsonIgnore
	public String getPasswordDecode() {
		return  new String(Base64.getDecoder().decode(password.get()));
	}
	public void setEmail(String email) {
		this.email = new SimpleStringProperty(email);
	}

	public void setSecondname(String secondname) {
		this.secondname = new SimpleStringProperty(secondname);
	}

	public void setFirstname(String firstname) {
		this.firstname = new SimpleStringProperty(firstname);
	}
	
	public void setLastname(String lastname) {
		this.lastname = new SimpleStringProperty(lastname);
	}
	
	public void setRole(String role) {
		this.role = new SimpleStringProperty(role);
	}
	
	public void setPassword(String password) {
		this.password = new SimpleStringProperty(password);
	}

	public void setSend(boolean send) {
		this.send = new SimpleBooleanProperty(send);
	}

	public String getFirstname() {
		return firstname.get();
	}

	public String getSecondname() {
		return secondname.get();
	}
	
	public String getRole() {
		return role.get();
	}
	
	public boolean getSend() {
		return this.send.get();
	}
	
	public String getPassword() {
		return  password.get();
	}
	
	public String getEmail() {
		return  email.get();
	}
	
	public String getLastname() {
		return lastname.get();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (!email.equals(other.email))
			return false;
		if (!firstname.equals(other.firstname))
			return false;
		if (!lastname.equals(other.lastname))
			return false;
		if (!password.equals(other.password))
			return false;
		if (!role.equals(other.role))
			return false;
		if (!secondname.equals(other.secondname))
			return false;
		if (!send.equals(other.send))
			return false;
		return true;
	}

	@JsonIgnore
	@Override
	public String toString() {
		return "User [firstname=" + firstname + ", lastname=" + lastname + ", role=" + role + ", email=" + email
				+ ", password=" + password + "]";
	}
}
