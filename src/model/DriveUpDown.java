package model;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

public class DriveUpDown {
	/*
	 * ³������ ��� ���� ��������� �����, ����� �������, �������� ��������
	 * ������ , ����� ��� ����� ������� �����������.
	 */
	private static final String APPLICATION_NAME = "rozklad";
	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"),
			".credentials/drive-java-quickstart");
	private static final String UPLOAD_FILE_PATH = System.getProperty("user.home") + "/chnuPM/";
	private static final String DIR_FOR_DOWNLOADS = System.getProperty("user.home") + "/chnuPM/";
	private static FileDataStoreFactory dataStoreFactory;
	private static HttpTransport httpTransport;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static Drive drive;

	/*
	 * ��� ������������ ��'���� ���������� �������� ���� �������. � �������,
	 * ���� �������� ��� ����� ������� �����, ���� ����������� ����������� ��
	 * ��������� ������ authorize().
	 */
	public DriveUpDown() {
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
			Credential credential = authorize();
			drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
					.build();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	/*
	 * ��������� ���� ������� �� ���������� �� ����� �������������� ����
	 * ����������� ����������.
	 */
	private static Credential authorize() throws Exception {
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
				new InputStreamReader(DriveUpDown.class.getResourceAsStream("/resources/client_secret.json")));
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY,
				clientSecrets, Collections.singleton(DriveScopes.DRIVE)).setDataStoreFactory(dataStoreFactory).build();
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	/*
	 * ����� ����� � ���� �� ������. ������� ������ ��������� ���� �� null.
	 */
	public File getFileByName(String name) throws IOException {
		// ����� ����� ����� � ���� ����� � ������� ��'��.
		FileList result = drive.files().list().setQ("name='" + name + "'")
				.setFields("nextPageToken, files(id, name,lastModifyingUser,modifiedTime)").execute();
		List<File> files = result.getFiles();
		if (files != null) {
			return files.get(0);
		} else
			return null;
	}

	/*
	 * ����� ��� ��������� ����� � ����.
	 */
	public File updateFile(String type, String name) {
		try {
			// ����� � ���� ����� � ������� ��'��.
			File file = getFileByName(name);
			File fil = new File();
			// ³������� ����� �� ��������� � ����� ����������.
			java.io.File fileContent = new java.io.File(UPLOAD_FILE_PATH + name);
			FileContent mediaContent = new FileContent(type, fileContent);
			// ������������ ���������� ����� �� ����.
			File updatedFile = drive.files().update(file.getId(), fil, mediaContent).execute();
			return updatedFile;
		} catch (IOException e) {
			System.out.println("An error occurred: " + e);
			return null;
		}
	}

	/*
	 * ����� ������� ��������� �� ����������� ����� ���� � ������� ��'�� � �������
	 * ����� ������������
	 */
	public boolean checkIsModified(String name, String type) throws Exception {
		//����� ����� �� ������ � ����
		File f = getFileByName(name);
		java.util.Date d = new java.util.Date(f.getModifiedTime().getValue());
		LocalDateTime was = d.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		System.out.println(f.getLastModifyingUser().getMe());
		//�������� ������������ ����� �� ������ ����, �� ���� ����� ����� ����� 
		if (!name.endsWith(".json") && !f.getLastModifyingUser().getMe())
			if (((LocalDateTime.now().getHour() - was.getHour()) < 0)
					&& (Math.abs((LocalDateTime.now().getMinute() - was.getMinute())) < 5)) {
				throw new Exception();
			}
		return false;
	}

	/*
	 * ����� �������� ����������� ���� � ����� � �������� �������.
	 */
	public boolean downloadFile(String name, String type) throws Exception {
		/*
		 * �������� ��������� ����� ��� �����������, � ������� 
		 * �� ��������� ������� ������� �����. 
		 */
		java.io.File parentDir = new java.io.File(DIR_FOR_DOWNLOADS);
		if (!parentDir.exists() && !parentDir.mkdirs()) {
			throw new IOException("Unable to create parent directory");
		}
		//����� ����� �� �����.
		File f = getFileByName(name);
		if (f == null)
			return false;

		java.util.Date d = new java.util.Date(f.getModifiedTime().getValue());
		LocalDateTime was = d.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		System.out.println(f.getLastModifyingUser().getMe());
		//�������� �� ����������� ����� ���� � ������� ����� ������������
		if (!name.endsWith(".json") && !f.getLastModifyingUser().getMe())
			if (((LocalDateTime.now().getHour() - was.getHour()) < 0)
					&& (Math.abs((LocalDateTime.now().getMinute() - was.getMinute())) < 5)) {
				throw new Exception();
			}
		//� ������� ���� ���� �� ��������, ���������� �������� ����� ���������� � ����.
		OutputStream out = new FileOutputStream(new java.io.File(parentDir, name));
		drive.files().get(f.getId()).executeMediaAndDownloadTo(out);
		return true;

	}

}
